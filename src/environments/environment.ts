// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD7Ywcuh189srxxuqCimoj-I_My8_sB-VM',
    authDomain: 'ng-fitness-tracker-4335c.firebaseapp.com',
    databaseURL: 'https://ng-fitness-tracker-4335c.firebaseio.com',
    projectId: 'ng-fitness-tracker-4335c',
    storageBucket: 'ng-fitness-tracker-4335c.appspot.com',
    messagingSenderId: '133098083039',
    appId: '1:133098083039:web:cfd9e4f866c851f3b74f6a'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
