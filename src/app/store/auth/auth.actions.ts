import { TypedAction } from '@ngrx/store/src/models';

export enum AuthActionType {
  SET_AUTHENTICATED = '[AUTH] Set authenticated',
  SET_UNAUTHENTICATED = '[AUTH] Set unauthenticated',
}

export class SetAuthenticated implements TypedAction<AuthActionType> {
  readonly type = AuthActionType.SET_AUTHENTICATED;
}

export class SetUnauthenticated implements TypedAction<AuthActionType> {
  readonly type = AuthActionType.SET_UNAUTHENTICATED;
}

export type AuthAction = SetAuthenticated | SetUnauthenticated;

