import {
  AuthAction,
  AuthActionType
} from './auth.actions';

export interface AuthState {
  isAuthenticated: boolean;
}

const initialState: AuthState = {
  isAuthenticated: false
};

export function authReducer(state = initialState, action: AuthAction): AuthState {
  switch (action.type) {
    case AuthActionType.SET_AUTHENTICATED:
      return {
        isAuthenticated: true
      };
    case AuthActionType.SET_UNAUTHENTICATED:
      return {
        isAuthenticated: false
      };
    default:
      return state;
  }
}

export const getIsAuthenticated = (state: AuthState) => state.isAuthenticated;
