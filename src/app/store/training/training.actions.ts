import { TypedAction } from '@ngrx/store/src/models';
import { Exercise } from '../../training/models/exercise.model';

export enum TrainingActionType {
  SET_AVAILABLE_TRAININGS = '[TRAINING] Set available trainings',
  SET_FINISHED_TRAININGS = '[TRAINING] Set finished trainings',
  START_TRAINING = '[TRAINING] Start training',
  STOP_TRAINING = '[TRAINING] Stop training',
}

export class SetAvailableTrainings implements TypedAction<TrainingActionType> {

  readonly type = TrainingActionType.SET_AVAILABLE_TRAININGS;

  constructor(readonly payload: Exercise[]) { }
}

export class SetFinishedTrainings implements TypedAction<TrainingActionType> {

  readonly type = TrainingActionType.SET_FINISHED_TRAININGS;

  constructor(readonly payload: Exercise[]) { }
}

export class StartTraining implements TypedAction<TrainingActionType> {

  readonly type = TrainingActionType.START_TRAINING;

  constructor(readonly payload: string) { }
}

export class StopTraining implements TypedAction<TrainingActionType> {

  readonly type = TrainingActionType.STOP_TRAINING;
}

export type TrainingAction = SetAvailableTrainings | SetFinishedTrainings | StartTraining | StopTraining;

