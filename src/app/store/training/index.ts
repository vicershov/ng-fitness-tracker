import * as TrainingActions from './training.actions';
import * as fromTraining from './training.reducer';

export {
  TrainingActions,
  fromTraining
};
