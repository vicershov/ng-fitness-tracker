import {
  createFeatureSelector,
  createSelector
} from '@ngrx/store';
import { Exercise } from '../../training/models/exercise.model';
import { fromRoot } from '../index';

import {
  TrainingAction,
  TrainingActionType
} from './training.actions';

export interface TrainingState {
  availableExercises: Exercise[];
  finishedExercises: Exercise[];
  activeTraining: Exercise;
}

// Because of lazy loading.
export interface State extends fromRoot.AppState {
  training: TrainingState;
}

const initialState: TrainingState = {
  availableExercises: [],
  finishedExercises: [],
  activeTraining: null
};

export function trainingReducer(state = initialState, action: TrainingAction): Partial<TrainingState> {
  switch (action.type) {
    case TrainingActionType.SET_AVAILABLE_TRAININGS:
      return {
        ...state,
        availableExercises: action.payload
      };
    case TrainingActionType.SET_FINISHED_TRAININGS:
      return {
        ...state,
        finishedExercises: action.payload
      };
    case TrainingActionType.START_TRAINING:
      return {
        ...state,
        activeTraining: { ...state.availableExercises.find((exercise) => action.payload === exercise.id) }
      };
    case TrainingActionType.STOP_TRAINING:
      return {
        ...state,
        activeTraining: null
      };
    default:
      return state;
  }
}

export const getTrainingState = createFeatureSelector<TrainingState>('training');

export const getAvailableTrainings = createSelector(getTrainingState, (state: TrainingState) => state.availableExercises);
export const getFinishedTrainings = createSelector(getTrainingState, (state: TrainingState) => state.finishedExercises);
export const getActiveTraining = createSelector(getTrainingState, (state: TrainingState) => state.activeTraining);
export const hasActiveTraining = createSelector(getTrainingState, (state: TrainingState) => !!state.activeTraining);
