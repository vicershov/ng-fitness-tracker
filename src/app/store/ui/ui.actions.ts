import { TypedAction } from '@ngrx/store/src/models';

export enum UiActionType {
  START_LOADING = '[UI] Start Loading',
  STOP_LOADING = '[UI] Stop Loading',
}

export class StartLoading implements TypedAction<UiActionType> {
  readonly type = UiActionType.START_LOADING;
}

export class StopLoading implements TypedAction<UiActionType> {
  readonly type = UiActionType.STOP_LOADING;
}

export type UiAction = StartLoading | StopLoading;

