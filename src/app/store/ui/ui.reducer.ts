import {
  UiAction,
  UiActionType
} from './ui.actions';

export interface UiState {
  isLoading: boolean;
}

const initialState: UiState = {
  isLoading: false
};

export function uiReducer(state = initialState, action: UiAction): UiState {
  switch (action.type) {
    case UiActionType.START_LOADING:
      return {
        isLoading: true
      };
    case UiActionType.STOP_LOADING:
      return {
        isLoading: false
      };
    default:
      return state;
  }
}

export const getIsLoading = (state: UiState) => state.isLoading;
