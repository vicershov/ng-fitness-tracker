import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store } from '@ngrx/store';
import {
  Subject,
  Subscription
} from 'rxjs';
import {
  map,
  take
} from 'rxjs/operators';
import { UiService } from '../shared/ui.service';
import {
  fromTraining,
  TrainingActions
} from '../store/training';
import { getActiveTraining } from '../store/training/training.reducer';
import { UiActions } from '../store/ui';
import { Exercise } from './models/exercise.model';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {

  exercisesUpdated = new Subject<Exercise[]>();
  finishedExercisesChanged = new Subject<Exercise[]>();

  private firebaseSubscriptions: Subscription[] = [];

  constructor(private uiService: UiService,
              private fireStore: AngularFirestore,
              private store: Store<fromTraining.State>) { }

  fetchAvailableExercises(): void {
    this.store.dispatch(new UiActions.StartLoading());
    const availableExercisesSubscription = this.fireStore.collection<Exercise>('available-exercises')
      .snapshotChanges()
      .pipe(
        map(docList => docList.map(doc => {
          return {
            ...doc.payload.doc.data(),
            id: doc.payload.doc.id
          } as Exercise;
        }))
      ).subscribe((exercises) => {
          this.store.dispatch(new UiActions.StopLoading());
          this.store.dispatch(new TrainingActions.SetAvailableTrainings(exercises));
        },
        (error) => {
          this.store.dispatch(new UiActions.StopLoading());
          this.uiService.showSnackBar('Fetching exercises failed, please retry later.', null);
          this.exercisesUpdated.next(null);
        }
      );
    this.firebaseSubscriptions.push(availableExercisesSubscription);
  }

  startExercise(exerciseId: string): void {
    this.store.dispatch(new TrainingActions.StartTraining(exerciseId));
  }

  fetchCompletedOrCancelledExercises(): void {
    const finishedExercisesSubscription = this.fireStore.collection<Exercise>('finished-exercises').valueChanges()
      .subscribe((exercises = []) => {
        this.store.dispatch(new TrainingActions.SetFinishedTrainings(exercises.map(exercise => {
          // @ts-ignore
          exercise.date = (exercise.date as unknown).toDate();
          return exercise;
        })));
      });
    this.firebaseSubscriptions.push(finishedExercisesSubscription);
  }

  completeExercise(): void {
    this.store.select(getActiveTraining)
      .pipe(take(1))
      .subscribe((exercise) => {
        this.saveToFirestore({
          ...exercise,
          date: new Date(),
          state: 'completed'
        });
        this.store.dispatch(new TrainingActions.StopTraining());
      });
  }

  cancelExercise(progress: number): void {
    this.store.select(getActiveTraining)
      .pipe(take(1))
      .subscribe((exercise) => {
        this.saveToFirestore({
          ...exercise,
          duration: exercise.duration * (progress / 100),
          calories: exercise.calories * (progress / 100),
          date: new Date(),
          state: 'completed'
        });
        this.store.dispatch(new TrainingActions.StopTraining());
      });
  }

  unsubscribeFromFirebase(): void {
    while (this.firebaseSubscriptions.length > 0) {
      this.firebaseSubscriptions.pop().unsubscribe();
    }
  }

  private saveToFirestore(exercise: Exercise): void {
    this.fireStore.collection('finished-exercises').add(exercise);
  }
}
