import {
  Component,
  OnInit
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { fromTraining } from '../../store/training';
import { getActiveTraining } from '../../store/training/training.reducer';

import { TrainingService } from '../training.service';
import {
  StopTrainingComponent,
  StopTrainingComponentDialogData
} from './stop-training.component';

@Component({
  selector: 'app-current-training',
  templateUrl: './current-training.component.html',
  styleUrls: [ './current-training.component.css' ]
})
export class CurrentTrainingComponent implements OnInit {

  progress = 0;

  private timeout;

  constructor(private dialog: MatDialog,
              private trainingService: TrainingService,
              private store: Store<fromTraining.State>) {
  }

  ngOnInit(): void {
    this.startOrResumeTimer();
  }

  onStop(): void {
    clearTimeout(this.timeout);
    this.dialog.open(StopTrainingComponent, {
      data: {
        progress: this.progress
      } as StopTrainingComponentDialogData
    }).afterClosed().subscribe((result) => {
      if (result) {
        this.trainingService.cancelExercise(this.progress);
      } else {
        this.startOrResumeTimer();
      }
    });
  }

  private startOrResumeTimer(): void {
    this.store.select(getActiveTraining)
      .pipe(take(1))
      .subscribe((exercise) => {
        const step = exercise.duration / 100 * 1000;
        this.timeout = setInterval(() => {
          this.progress += 1;
          if (this.progress >= 100) {
            this.trainingService.completeExercise();
            clearTimeout(this.timeout);
          }
        }, step);
      });
  }
}
