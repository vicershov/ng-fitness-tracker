import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  take,
  tap
} from 'rxjs/operators';
import { fromRoot } from '../store';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private store: Store<fromRoot.AppState>,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.store.select(fromRoot.getIsAuthenticated)
      .pipe(
        take(1),
        tap((can) => {
          if (!can) {
            this.router.navigate([ 'login' ]);
          }
        }));
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> {
    return this.store.select(fromRoot.getIsAuthenticated).pipe(
      take(1),
      tap((can) => {
        if (!can) {
          this.router.navigate([ 'login' ]);
        }
      }));
  }
}
