import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { UiService } from '../shared/ui.service';
import * as fromRoot from '../store/app.reducer';
import { AuthActions } from '../store/auth';
import * as UiActions from '../store/ui/ui.actions';
import { TrainingService } from '../training/training.service';
import { AuthData } from './models/auth-data';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router,
              private fireAuth: AngularFireAuth,
              private uiService: UiService,
              private trainingService: TrainingService,
              private store: Store<fromRoot.AppState>) { }

  initAuthListener(): void {
    this.fireAuth.authState.subscribe((user) => {
      if (user) {
        this.store.dispatch(new AuthActions.SetAuthenticated());
        this.router.navigate([ 'training' ]);
      } else {
        this.trainingService.unsubscribeFromFirebase();
        this.store.dispatch(new AuthActions.SetUnauthenticated());
      }
    });
  }

  registerUser(authData: AuthData): void {
    this.store.dispatch(new UiActions.StartLoading());
    this.fireAuth.createUserWithEmailAndPassword(authData.email, authData.password)
      .catch((error) => {
        this.uiService.showSnackBar(error.message, null);
      })
      .finally(() => {
        this.store.dispatch(new UiActions.StopLoading());
      });
  }

  login(authData: AuthData): void {
    this.store.dispatch(new UiActions.StartLoading());
    this.fireAuth.signInWithEmailAndPassword(authData.email, authData.password)
      .catch((error) => {
        this.uiService.showSnackBar(error.message, null);
      })
      .finally(() => {
        this.store.dispatch(new UiActions.StopLoading());
      });
  }

  logout(): void {
    this.fireAuth.signOut();
    this.router.navigate([ 'login' ]);
  }
}
